#!/usr/bin/env bash

#./live.sh avector

showwaves="
[0:a]highpass=f=100[a1];
[a1]lowpass=f=15000[a2];
[a2]arnndn=m='rnnoise-models/marathon-prescription-2018-08-29/mp.rnnn'[a3];
[a3]agate=threshold=-25dB:ratio=9000[a4];
[a4]showwaves=split_channels=1:mode=line:colors=white@1|yellow@1:s=1280x480[vf];
color=s=1280x720:c=black[bg];
[bg][vf]overlay=x=0:y=(H-h)/2[v]
"

avectorscope="
[0:a]highpass=f=100[a1];
[a1]lowpass=f=15000[a2];
[a2]arnndn=m='rnnoise-models/marathon-prescription-2018-08-29/mp.rnnn'[a3];
[a3]agate=threshold=-25dB:ratio=9000[a4];
[a4]avectorscope=s=1280x720:zoom=5:draw=line:scale=sqrt:m=polar[vf];
color=s=1280x720:c=black[bg];
[bg][vf]overlay[v]
"

showfreqs="
[0:a]highpass=f=100[a1];
[a1]lowpass=f=15000[a2];
[a2]arnndn=m='rnnoise-models/marathon-prescription-2018-08-29/mp.rnnn'[a3];
[a3]agate=threshold=-25dB:ratio=9000[a4];
[a4]showfreqs=s=1280x720:colors=white@1|white@1[vf];
color=s=1280x720:c=black[bg];
[bg][vf]overlay[v]
"

showvolume="
[0:a]highpass=f=100[a1];
[a1]lowpass=f=15000[a2];
[a2]arnndn=m='rnnoise-models/marathon-prescription-2018-08-29/mp.rnnn'[a3];
[a3]agate=threshold=-25dB:ratio=9000[a4];
[a4]showvolume=p=0.1,scale=1280:40[vf];
color=s=1280x720:c=black[bg];
[bg][vf]overlay=x=0:y=(H-h)/2[v]
"

current=$showfreqs

if [ $1 == 'waves' ]
then
    current=$showwaves
elif [ $1 == 'avector' ]
then
    current=$avectorscope
elif [ $1 == 'freqs' ]
then
    current=$showfreqs
elif [ $1 == 'volume' ]
then
    current=$showvolume
fi

ffmpeg -y -f pulse -i default -filter_complex "$current" -map '[v]' -f matroska -preset ultrafast - | ffplay -
