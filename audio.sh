#!/usr/bin/env bash

#./audio.sh audio.flac output.mp3

ffmpeg -y -i "$1" -af "
adeclip,
highpass=f=50,
lowpass=f=15000,
agate=threshold=-40dB:ratio=8000,
asendcmd=0.0 afftdn sn start,
asendcmd=0.3 afftdn sn stop,
afftdn=nf=-40,
adeclick,
deesser,
loudnorm=i=-16,
silenceremove=start_periods=1:start_threshold=-40dB:start_silence=0.1
" -c:a mp3 -b:a 320k "$2"
