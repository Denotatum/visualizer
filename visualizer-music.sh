#!/usr/bin/env bash

#./visualizer-music.sh audio.flac bg.png info.png intro.mp4 output.mp4

time ffmpeg -y -i "$1" -i "$2" -i "$3" -filter_complex "
[0:a]avectorscope=s=1920x1080:zoom=2:draw=line:scale=sqrt:m=lissajous[vav];
[0:a]showfreqs=s=1920x1080:colors=white@.5|white@0.5[vf];
[0:a]showfreqs=s=1920x1080:colors=black@.5|black@0.5[vf2];
[1:v]scale=1920:1080[bg];
[2:v]colorchannelmixer=aa=0.3[info];
[vf]vflip[vff];
[vff]hflip[vfa];
[vf2][vfa]overlay[v3];
[v3][vav]overlay[v4];
[bg][v4]overlay[v5];
[v5][info]overlay[v6];
movie=$4[vi];
amovie=$4[ai];
[vi][v6]concat[v];
[ai][0:a]concat=v=0:a=1[a]
" -map '[v]' -map '[a]' -c:a mp3 -b:a 320k -f matroska -preset slow "$5"
 
