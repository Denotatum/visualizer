It's audio utils for make audio/video podcast.

## Requirements

* time
* ffmpeg
* kdialog (for `gui.sh`)

And RNNoise models:

```sh
git clone https://github.com/GregorR/rnnoise-models.git
```

For `live.sh` script.

## Scripts

* `gui.sh`: GUI for generate audio and video
* `audio.sh`: audio processor
* `live.sh`: show live visualization (with ffplay, 1280x720)
* `record.sh`: recorder from pulse microphone
* `visualizer.sh`: horizontal visualizer (1920x1080)
* `visualizer-vertical.sh`: vertical visualizer (1080x1920)
* `visualizer-music.sh`: like a `visualizer.sh`, for music (other filters)

## Examples

### GUI

```sh
./gui.sh
```

For run GUI mode. Require `kdialog`.

### Horizontal visualizer

```sh
./visualizer.sh audio.flac bg.png info.png intro.mp4 output.mp4
```

Arguments:

1. audio file for visualization
2. background image
3. info image (its will on top and transparent)
4. intro video file
5. path to output result file

### Vertical visualizer

```sh
./visualizer-vertical.sh audio.flac bg.png info.png output.mp4
```

Arguments:

1. audio file for visualization
2. background image
3. info image (its will on top and transparent)
4. path to output result file

### Audio processor

```sh
./audio.sh audio.flac output.mp3
```

Process audio file (denoise, normalize and etc).

### Recorder

```sh
./record.sh output.flac
```

Record from microphone with 3 second start pause.

### Live visualization

```sh
./live.sh waves
```

Show live visualization in 1280x720 window. Background: black. Arguments:

* **waves**: showwaves
* **avector**: avectorscope
* **freqs**: showfreqs
* **volume**: showvolume

Default: **freqs**

## License

GNU GPL v3
