#!/usr/bin/env bash

path=$(echo $0 | rev | sed -r 's/^[^/]+//' | cut -c 2- | rev)
echo "choice format..."
vertical=$(kdialog --title "Video is vertical?" --yesno "Generate video is vertical?" && echo 1)
music=

if [ ! $vertical ]
then
    music=$(kdialog --title "Video is music?" --yesno "Generate video for music?" && echo 1)
fi

echo "select audio file..."
audio=$(kdialog --getopenfilename ~/ --title "Main audio" "audio/ogg audio/wav audio/aac audio/mpeg audio/ogg audio/vorbis audio/mp4 audio/flac")

if [ -z $audio ]
then
    echo "File not select. Exit."
    exit 0
fi

echo "set audio processing..."
aproc=$(kdialog --title "Process audio" --yesno "Process audio?" && echo 1)
aout=""

if [ $aproc ]
then
    echo "select output audio file..."
    aout=$(kdialog --getsavefilename ~/ --title "Save ouput audio" "audio/mp3")

    if [ -z $aout ]
    then
        echo "File not select. Exit."
        exit 0
    fi
fi

echo "select bg image..."
bg=$(kdialog --getopenfilename ~/ --title "Background image" "image/jpeg image/pjpeg image/png")

if [ -z $bg ]
then
    echo "File not select. Exit."
    exit 0
fi

echo "select overlay image..."
info=$(kdialog --getopenfilename ~/ --title "Info (overlay) image" "image/jpeg image/pjpeg image/png")

if [ -z $info ]
then
    echo "File not select. Exit."
    exit 0
fi

intro=""

if [ ! $vertical ]
then
    echo "select intro video..."
    intro=$(kdialog --getopenfilename ~/ --title "Intro video" "video/mpeg video/mp4 video/ogg video/x-msvideo video/x-matroska")
    
    if [ -z $intro ]
    then
        echo "File not select. Exit."
        exit 0
    fi
fi

echo "select output video file..."
outfile=$(kdialog --getsavefilename ~/ --title "Save ouput video" "video/mp4")

if [ -z $outfile ]
then
    echo "File not select. Exit."
    exit 0
fi


if [ $vertical ]
then
    echo "Format: vertical (1080x1920)"
else
    echo "Format: horizontal (1920x1080)"
fi

if [ $music ]
then
    echo "Music: yes"
else
    echo "Music: no"
fi

echo "Audio for visualization:" "$audio"
echo "Background image:" "$bg"
echo "Info (overlay) image:" "$info"
echo "Intro video:" "$intro"
echo "Save audio into:" "$aout"
echo "Save video into:" "$outfile"

if [ $aproc ]
then
    echo "Run:" $path"/audio.sh" "$audio" "$aout"
    source $path/audio.sh "$audio" "$aout"
    audio=$aout
fi

if [ $music ]
then
    echo "Run:" $path"/visualizer-music.sh" "$audio" "$bg" "$info" "$intro" "$outfile"
    source $path/visualizer-music.sh "$audio" "$bg" "$info" "$intro" "$outfile"
    exit 0
fi

if [ $vertical ]
then
    echo "Run:" $path"/visualizer-vertical.sh" "$audio" "$bg" "$info" "$outfile"
    source $path/visualizer-vertical.sh "$audio" "$bg" "$info" "$outfile"
else
    echo "Run:" $path"/visualizer.sh" "$audio" "$bg" "$info" "$intro" "$outfile"
    source $path/visualizer.sh "$audio" "$bg" "$info" "$intro" "$outfile"
fi
