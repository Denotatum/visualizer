#!/usr/bin/env bash

#./visualizer.sh audio.flac bg.png info.png intro.mp4 output.mp4

time ffmpeg -y -i "$1" -i "$2" -i "$3" -filter_complex "
[0:a]avectorscope=s=1920x1080:zoom=2:draw=line:scale=sqrt:m=polar[vav];
[0:a]showvolume=p=0.1,scale=1920:40[vv];
[0:a]showwaves=split_channels=1:mode=line:colors=white@1|yellow@1:s=1920x480[vs];
[0:a]showfreqs=s=1920x1080:colors=blue@.1|blue@0.1[vf];
[0:a]showfreqs=s=1920x1080:colors=indigo@.1|indigo@0.1[vf2];
[1:v]scale=1920:1080[bg];
[2:v]colorchannelmixer=aa=0.3[info];
[vs][vv]overlay=x=0:y=(H-h)/2[v1];
[vf]vflip[vff];
[vff]hflip[vfa];
[vfa][v1]overlay=y=50[v2];
[vf2][v2]overlay[v3];
[vav][v3]overlay[v4];
[bg][v4]overlay[v5];
[v5][info]overlay[v6];
movie=$4[vi];
amovie=$4[ai];
[vi][v6]concat[v];
[ai][0:a]concat=v=0:a=1[a]
" -map '[v]' -map '[a]' -c:a mp3 -b:a 320k -f matroska -preset slow "$5"
